module.exports = function (sequelize, DataTypes) {

	var Incident = sequelize.define('Incident', {
		details: DataTypes.STRING,
		lat: DataTypes.FLOAT,
		lon: DataTypes.FLOAT
	}, {
		classMethods: {
			associate: function (models) {
				Incident.belongsTo(models.User);
				Incident.belongsTo(models.IncidentType);
				Incident.belongsTo(models.District);
			}
		}
	});

	return Incident;
};