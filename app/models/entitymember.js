module.exports = function (sequelize, DataTypes) {

	var EntityMember = sequelize.define('EntityMember', {
		role: DataTypes.STRING
	}, {
		classMethods: {
			associate: function (models) {
				// example on how to add relations
				// EntityMember.hasMany(models.Comments);
			}
		}
	});

	return EntityMember;
};