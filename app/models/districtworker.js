module.exports = function (sequelize, DataTypes) {

	var DistrictWorker = sequelize.define('DistrictWorker', {
		role: DataTypes.STRING
	}, {
		classMethods: {
			associate: function (models) {
				// example on how to add relations
				// DistrictWorker.hasMany(models.Comments);
			}
		}
	});

	return DistrictWorker;
};