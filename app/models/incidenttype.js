module.exports = function (sequelize, DataTypes) {

	var IncidentType = sequelize.define('IncidentType', {
		name: DataTypes.STRING
	}, {
		classMethods: {
			associate: function (models) {
				IncidentType.belongsToMany(models.Entity, {
					through: 'EntityIncidentType'
				});
			}
		}
	});

	return IncidentType;
};