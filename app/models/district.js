module.exports = function (sequelize, DataTypes) {

	var District = sequelize.define('District', {
		name: DataTypes.STRING,
	}, {
		classMethods: {
			associate: function (models) {
				District.belongsToMany(models.User, {
					through: models.DistrictWorker,
					as: 'Worker'
				});
				District.hasMany(models.Entity);
			}
		}
	});

	return District;
};