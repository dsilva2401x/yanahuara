module.exports = function (sequelize, DataTypes) {

	var Entity = sequelize.define('Entity', {
		name: DataTypes.STRING
	}, {
		classMethods: {
			associate: function (models) {
				Entity.belongsToMany(models.User, {
					through: models.EntityMember,
					as: 'Member'
				});
				Entity.belongsToMany(models.IncidentType, {
					through: 'EntityIncidentType'
				});
				// models.Entity.belongsTo(models.District);
			}
		}
	});

	return Entity;
};