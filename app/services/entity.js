var models = require('../models');

module.exports = function (router) {
	
	// GET
		router.get('/entity', function (req, res, next) {
			models.Entity.findAll().then(function (entities) {
				res.json(entities);
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

		router.get('/entity/:entityId', function (req, res, next) {
			models.Entity.findOne( req.params.entityId ).then(function (entity) {
				res.json(entity);
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

		router.get('/entity/:entityId/member', function (req, res, next) {
			models.Entity.findOne( req.params.entityId ).then(function (entity) {
				entity.getMember().then(function (members) {
					res.json(members);
				}).catch(function (error) {
					console.log(error);
					res.end();
				});
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

		router.get('/entity/:entityId/member/:memberId', function (req, res, next) {
			var entityId = req.params.entityId,
				memberId = req.params.memberId;

			models.EntityMember.findOne({
				where: {
					UserId: memberId,
					EntityId: entityId
				}
			}).then(function (member) {
				console.log(member);
				// TODO
				res.end();
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

	// POST
		router.post('/entity', function (req, res, next) {
			var data = req.body;
			models.Entity.create({
				name: data.name,
				DistrictId: data.districtId || null
			}).then(function (entity) {
				res.json(entity);
			}).catch(function (error) {
				console.log( error );
				res.end();
			});			
		});

	// PUT
		router.put('/entity/:entityId', function (req, res, next) {
			var data = req.body;
			models.Entity.findOne( req.params.entityId ).then(function (entity) {
				entity.name = data.name;
				entity.DistrictId = data.districtId || null;
				entity.save().then(function () {
					res.json(entity);
				}).catch(function (error) {
					console.log(error);
					res.end();
				});
			}).catch(function (error) {
				console.log(error);
				res.end();
			});		
		});

	// DELETE
		router.delete('/entity/:entityId', function (req, res, next) {
			models.Entity.findOne( req.params.entityId ).then(function (entity) {
				entity.destroy().then(function() {
					res.json(entity);
				}).catch(function (error) {
					console.log(error);
					res.end();
				});
			}).catch(function (error) {
				console.log(error);
				res.end();
			});		
		});

}