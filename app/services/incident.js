var models = require('../models');

module.exports = function (router) {
	
	// GET
		router.get('/incident', function (req, res, next) {
			models.Incident.findAll().then(function (incidents) {

				res.json(incidents);
				
			});
		});

		router.get('/incident/:incidentId', function (req, res, next) {
			models.Incident.findOne( req.params.incidentId ).then(function (incident) {

				res.json(incident);
				
			});
		});

		router.post('/incident', function (req, res, next) {

			models.Incident.create({
				details: req.body.details,
				lat: req.body.incidentLocation.latitude,
				lon: req.body.incidentLocation.longitude
			}).then(function (incident) {
				console.log(incident);
			});

			res.end();
			/*console.log( req.body );
			res.end()*/

		});

}