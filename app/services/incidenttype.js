var models = require('../models');

module.exports = function (router) {
	
	// GET
		router.get('/incidenttype', function (req, res, next) {
			models.IncidentType.findAll().then(function (incidenttypes) {
				res.json(incidenttypes);
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

		router.get('/incidenttype/:incidenttypeId', function (req, res, next) {
			models.IncidentType.findOne( req.params.incidenttypeId ).then(function (incidenttype) {
				res.json(incidenttype);
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

		router.get('/incidenttype/:incidenttypeId/incident', function (req, res, next) {
			models.Incident.findAll({
				where: {
					IncidentTypeId: req.params.incidenttypeId
				}
			}).then(function (incidents) {
				res.json(incidents);
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

		router.get('/incidenttype/:incidenttypeId/entity', function (req, res, next) {
			models.EntityIncidentType.findAll({
				where: {
					IncidentTypeId: req.params.incidenttypeId
				}
			}).then(function (rels) {
				// TODO
				res.end();
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

	// POST
		router.post('/incidenttype', function (req, res, next) {
			var data = req.body;
			models.IncidentType.create({
				name: data.name
			}).then(function (entity) {
				res.json(entity);
			}).catch(function (error) {
				console.log( error );
				res.end();
			});
		});

	// PUT
		router.put('/incidenttype/:incidenttypeId', function (req, res, next) {
			var data = req.body;
			models.IncidentType.findOne( req.params.incidenttypeId ).then(function (incidenttype) {
				incidenttype.name = data.name;
				incidenttype.save().then(function () {
					res.json(incidenttype);
				}).catch(function (error) {
					console.log(error);
					res.end();
				});
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

	// DELETE
		router.delete('/incidenttype/:incidenttypeId', function (req, res, next) {
			models.IncidentType.findOne( req.params.incidenttypeId ).then(function (incidenttype) {
				incidenttype.destroy().then(function() {
					res.json(incidenttype);
				}).catch(function (error) {
					console.log(error);
					res.end();
				});
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

}