var models = require('../models');

module.exports = function (router) {
	
	// GET
		router.get('/district', function (req, res, next) {
			models.District.findAll().then(function (districts) {
				res.json(districts);
			}).catch(function (error) {
				console.log( error );
				res.end();
			});
		});

		router.get('/district/:districtId', function (req, res, next) {
			models.District.findOne( req.params.districtId ).then(function (district) {
				res.json(district);
			}).catch(function (error) {
				console.log( error );
				res.end();
			});
		});

		router.get('/district/:districtId/entity', function (req, res, next) {
			var districtId = req.params.districtId;
			models.District.findOne( req.params.districtId ).then(function (district) {
				district.getEntities().then(function (entities) {
					res.json(entities);
				}).catch(function (error) {
					console.log(error);
					res.end();
				});
			}).catch(function (error) {
				console.log( error );
				res.end();
			});
		});

		router.get('/district/:districtId/worker', function (req, res, next) {
			var districtId = req.params.districtId;
			models.District.findOne( req.params.districtId ).then(function (district) {
				district.getWorker().then(function (workers) {
					res.json(workers);
				}).catch(function (error) {
					console.log(error);
					res.end();
				})
			}).catch(function (error) {
				console.log( error );
				res.end();
			});
		});

		router.get('/district/:districtId/worker/:workerId', function (req, res, next) {
			var districtId = req.params.districtId,
				workerId = req.params.workerId;

			models.DistrictWorker.findOne({
				where: {
					UserId: workerId,
					DistrictId: districtId
				}
			}).then(function (worker) {
				console.log(worker);
				// TODO
				res.end();
			}).catch(function (error) {
				console.log(error);
				res.end();
			});
		});

		router.get('/district/:districtId/incident', function (req, res, next) {
			var districtId = req.params.districtId;
			models.Incident.findAll({
				where: {
					DistrictId: districtId
				}
			}).then(function (incidents) {
				res.json(incidents);
			}).catch(function (error) {
				console.log(error);
				res.end();
			})
		});

	// POST
		router.post('/district', function (req, res, next) {
			var data = req.body;
			models.District.create({
				name: data.name
			}).then(function (district) {
				res.json(district);
			}).catch(function (error) {
				console.log( error );
				res.end();
			});
		});

	// PUT
		router.put('/district/:districtId', function (req, res, next) {
			var data = req.body;
			models.District.findOne( req.params.districtId ).then(function (district) {
				district.name = data.name;
				district.save().then(function (district) {
					res.json(district);
				}).catch(function (error) {
					console.log( error );
					res.end();
				});
			}).catch(function (error) {
				console.log( error );
				res.end();
			});
		});

	// DELETE
		router.delete('/district/:districtId', function (req, res, next) {
			models.District.findOne( req.params.districtId ).then(function (district) {
				district.destroy().then(function() {
					res.json(district);
				}).catch(function (error) {
					console.log(error);
					res.end();
				});
			}).catch(function (error) {
				console.log( error );
				res.end();
			});
		});

}