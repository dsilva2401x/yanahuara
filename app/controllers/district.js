var express = require('express'),
	router = express.Router(),
	getModuleFiles = require('../../useful/getModuleFiles'),
	models = require('../models');

module.exports = function (app) {
	app.use('/district', router);
};

router.get('/:districtId', function (req, res, next) {
	models.District.findOne( req.params.districtId ).then(function (district) {
		res.render('material-angular-layout', {
			title: district.name,
			module: 'district',
			scripts: getModuleFiles('district'),
			districtId: district.id
		});
	}).catch(function (error) {
		console.log(error);
		res.end();
	})
});