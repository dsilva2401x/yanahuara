var express = require('express'),
	router = express.Router(),
	getModuleFiles = require('../../useful/getModuleFiles'),
	models = require('../models');

module.exports = function (app) {
	app.use('/districts', router);
};

router.get('/', function (req, res, next) {
	res.render('material-angular-layout', {
		title: 'districts',
		module: 'districts',
		scripts: getModuleFiles('districts')
	});
});
