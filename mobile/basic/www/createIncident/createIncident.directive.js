(function(ang) {

	var app = ang.module('app');

	app.directive('createIncident', [function() {
		return {
			restrict: 'EA',
			templateUrl: 'createIncident/createIncident.html',
			controller: function ($scope, $http) {
				var domain = prompt('Ingresar ip');
				$scope.submit = function () {
					// console.log( $scope )
					// var domain = 'http://localhost:3000'
					// var domain = 'http://192.168.160.195';
					$http({
						url: domain+'/api/incident',
						method: 'POST',
						data: $scope.models
					}).success(function (incident) {
						console.log(incident);
					})
				}
			}
		};
	}]);

})(angular)