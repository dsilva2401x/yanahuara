(function(ang) {

	var app = ang.module('app');

	app.directive('gmaps', [function() {
		return {
			restrict: 'EA',
			link: function (scope, elem, attrs) {

				var mapOptions = {
					zoom: 16,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(elem[0],mapOptions);
				var pos = new google.maps.LatLng(-16.3951358,-71.535288);
				// var defaultPosition = new google.maps.LatLng(-16.3951358,-71.535288);
				if(navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(myPosition){
						// var myPos = new google.maps.LatLng(myPosition.coords.latitude,myPosition.coords.longitude);
						pos = new google.maps.LatLng(myPosition.coords.latitude,myPosition.coords.longitude);
						// map.setCenter(myPos);
					},function() {
						// map.setCenter( defaultPosition );
					});
				}else{
					// map.setCenter( defaultPosition );
				}
				// console.log( pos.A, pos.F );
				map.setCenter( pos );

				var marker = new google.maps.Marker({
					position: pos,
					map: map,
					draggable: true
				});

				scope.models.incidentLocation = {
					latitude: pos.A,
					longitude: pos.F
				}
				google.maps.event.addListener(marker,'dragend',function(data){
					scope.models.incidentLocation = {
						latitude: data.latLng.A,
						longitude: data.latLng.F
					};
				});
				// dragend
				marker.setMap(map);

			}
		};
	}]);

})(angular)