(function(ang) {

	var app = ang.module('app');

	app.controller('listController', ['$scope', '$http', '$mdToast', '$state', function( $scope, $http, $mdToast ) {
		if (!$scope.models) $scope.models = {};
		if (!$scope.methods) $scope.methods = {};

		// Methods
			$scope.methods.loadDistricts = function() {
				$http({
					url: '/api/district',
					method: 'GET'
				}).then(function (resp) {
					if( !resp || resp.status!=200 || !resp.data ) {
						$scope.methods.showNotification('Error en el servidor');
						return;
					}
					$scope.models.districts = resp.data;
				});
			}

		// Init
			$scope.methods.loadDistricts();

	}]);

})(angular)