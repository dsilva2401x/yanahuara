(function(ang) {

	var app = ang.module('app');

	app.config( ['$mdThemingProvider', '$urlRouterProvider', '$stateProvider', function( $mdThemingProvider, $urlRouterProvider, $stateProvider ) {

		$mdThemingProvider.theme('default').primaryPalette('blue');

		$urlRouterProvider.otherwise('/create');

		$stateProvider
			.state('create', {
				url: '/create',
				templateUrl: '/modules/districts/create/create.html',
				controller: 'createController'
			})
			.state('list', {
				url: '/list',
				templateUrl: '/modules/districts/list/list.html',
				controller: 'listController'
			})
			.state('show', {
				url: '/show',
				templateUrl: '/modules/districts/show/show.html'
			})
			.state('modify', {
				url: '/modify',
				templateUrl: '/modules/districts/modify/modify.html'
			});

	}]);

})(angular)