(function(ang) {

	var app = ang.module('app');

	app.controller('createController', ['$scope', '$http', '$mdToast', '$state', function( $scope, $http, $mdToast ) {
		if (!$scope.models) $scope.models = {};
		if (!$scope.methods) $scope.methods = {};

		// Methods
			$scope.methods.showNotification = function(text) {
				$mdToast.show(
					$mdToast.simple()
						.content(text)
						.position('top right')
						.hideDelay(3000)
				);
			}

			$scope.methods.submit = function() {
				$http({
					url: '/api/district',
					method: 'POST',
					data: $scope.models.district
				}).then(function (resp) {
					if( !resp || resp.status!=200 ) {
						$scope.methods.showNotification('Error en el servidor');
						return;
					}
					if( !resp.data ) {
						$scope.methods.showNotification('Error en el registro');
					}
					$scope.methods.showNotification('Registro exitoso!');
					console.log( resp.data );
				});
			}

	}]);

})(angular)