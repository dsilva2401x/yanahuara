(function(ang) {

	var app = ang.module('app');

	app.controller('incidentsController', ['$scope', '$mdSidenav', '$http', function( $scope, $mdSidenav, $http ) {
		$scope.models = {};
		$scope.methods = {};
		// Methods		

		// Init
			$scope.methods.loadIncidents = function(callback) {
				$http({
					url: '/api/incident',
					method: 'GET'
				}).success(function (incidents) {
					callback(incidents);
				});
				
			}

	}]);

})(angular)