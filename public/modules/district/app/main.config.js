(function(ang) {

	var app = ang.module('app');

	app.config( ['$mdThemingProvider', '$urlRouterProvider', '$stateProvider', function( $mdThemingProvider, $urlRouterProvider, $stateProvider ) {

		$mdThemingProvider.theme('default').primaryPalette('blue');

		$urlRouterProvider.otherwise('/incidents');

		$stateProvider
			.state('incidents', {
				url: '/incidents',
				templateUrl: '/modules/district/incidents/incidents.html',
				controller: 'incidentsController'
			});

	}]);

})(angular)