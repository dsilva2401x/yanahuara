(function(ang) {
	var app = ang.module('app');
	app.directive('gmaps', [function($http) {
		return {
			restrict: 'EA',
			link: function (scope, elem) {
				
				var mapOptions = {
					zoom: 16,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(elem[0],mapOptions);
				var pos = new google.maps.LatLng(-16.3951358,-71.535288);
				if(navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(myPosition){
						pos = new google.maps.LatLng(myPosition.coords.latitude,myPosition.coords.longitude);
					},function() {
					});
				}
				map.setCenter( pos );
				scope.methods.loadIncidents(function (incidents) {
					incidents.forEach(function (i) {
						var markerPosition = new google.maps.LatLng( i.lat , i.lon );
						var marker = new google.maps.Marker({
							position: markerPosition,
							map: map,
							title: i.details
						});
						marker.setMap(map);
					});
				});
			}
		};
	}]);

})(angular)